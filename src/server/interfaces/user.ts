export interface IUpdateUserPayload {
    firstName?: string
    lastName?: string
}
