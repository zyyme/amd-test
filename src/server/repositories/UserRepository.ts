import { Op, Transaction, WhereOptions } from 'sequelize'
import { User, } from '../database/models';
import { UserStatus, } from '../enums';
import { IUpdateUserPayload } from '../interfaces/user'
import { Errors, ErrorsMessages, Exception } from '../utils'

interface IFindByEmailOptions {
	transaction?: Transaction;
}

interface IFindByLoginOptions {
	transaction?: Transaction;
	scope?: string;
}

interface ICreateOptions {
	transaction?: Transaction;
}

interface IFindAllQuery {
	id?: string
	email?: string
	firstName?: string
	lastName?: string
	createdAt?: Record<string, Date>
}

interface IPagination {
	page: number
	limit: number
}

export class UserRepository {
	static async findAll(query: IFindAllQuery, pagination: IPagination): Promise<{ users: User[], count: number }> {
		const offset = (pagination.page - 1) * pagination.limit

		const whereQuery = query as WhereOptions<any>
		const dbQuery = {
			where: whereQuery,
			offset,
			limit: pagination.limit
		}

		const users = await User.findAll(dbQuery)
		const count = await User.count({ where: whereQuery })

		return { users, count }
	}

	static async findByEmail(email: string, options: IFindByEmailOptions = {}): Promise<User | null> {
		const { transaction, } = options;

		return User.findOne({
			where: {
				email,
			},
			transaction,
		});
	}

	static async findByLogin(
		login: string,
		options: IFindByLoginOptions = {}
	): Promise<User | null> {
		const { transaction, scope = 'defaultScope', } = options;

		return User.scope(scope).findOne({
			where: {
				[Op.or]: [
					{
						email: login,
					},
					{
						phone: login,
					}
				],
			},
			transaction,
		});
	}

	static async findById(id: string): Promise<User | null> {
		return await User.findByPk(id)
	}

	static async update(id: string, data: IUpdateUserPayload) {
		const user = await this.findById(id)

		if (user) {
			return User.update(data, { where: { id } })
		}

		throw new Exception(Errors.UserNotFound, ErrorsMessages[Errors.UserNotFound])
	}

	static async create(
		values: Partial<User>,
		options: ICreateOptions = {}
	): Promise<User | null> {
		const { transaction, } = options;

		return User.create({
			...values,
			status: UserStatus.Active,
		}, {
			transaction,
		});
	}
}
