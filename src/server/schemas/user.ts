import { paginationSchema, stringSchema } from './common'
import Joi from 'joi'


export const getByEmailSchema = paginationSchema.keys({ email: stringSchema })

export const getByIdSchema = Joi.object({
    userId: Joi.string()
})

export const updateUserSchema = Joi.object({
    firstName: Joi.string().optional(),
    lastName: Joi.string().optional()
})
