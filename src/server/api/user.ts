import * as Hapi from '@hapi/hapi';
import { IOutputEmpty, IOutputOk, IOutputPagination } from '../interfaces'
import { User } from '../database/models'
import { Boom } from '@hapi/boom'
import { Errors, ErrorsMessages, Exception, handlerError, outputOk, outputPagination } from '../utils'
import { Op } from 'sequelize'
import { IUpdateUserPayload } from '../interfaces/user'
import { UserRepository } from '../repositories'

export async function getAll(r: Hapi.Request): Promise<IOutputEmpty | IOutputPagination<User[]> | Boom> {
    const { email, page, limit } = r.query

    let findQuery = {}
    if (email) {
        findQuery = { email }
    }

    const { users, count } = await UserRepository.findAll(findQuery, { page, limit })

    return outputPagination(count, users)
}

export async function getById(r: Hapi.Request): Promise<IOutputEmpty | IOutputOk<User> | Boom> {
    const id = r.params['userId']

    try {
        const user = await UserRepository.findById(id)

        if (user) {
            return outputOk(user)
        }

        throw new Exception(Errors.UserNotFound, ErrorsMessages[Errors.UserNotFound])
    } catch (err) {
        return handlerError('User not found', err)
    }
}

export async function updateUser(r: Hapi.Request): Promise<IOutputOk<{ success: boolean }> | Boom> { // todo: interface for data type
    const id = r.params['userId']
    const data = r.payload as IUpdateUserPayload

    try {
        await UserRepository.update(id, data)
        return outputOk({ success: true })
    } catch (err) {
        return handlerError('User not found', err)
    }
}

export async function getNewUsersCount(): Promise<IOutputOk<{ newUsers: number }> | Boom> {
    const pastMonth = new Date()
    pastMonth.setMonth(pastMonth.getMonth() - 1)
    const { count } = await UserRepository.findAll({
        createdAt: {
            [Op.gte]:  pastMonth
        }
    }, {
        page: 1,
        limit: 1
    })
    return outputOk({ newUsers: count })
}
