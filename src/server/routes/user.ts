import { ServerRoute } from '@hapi/hapi'
import * as api from '../api/user'
import { getByEmailSchema, getByIdSchema, updateUserSchema } from '../schemas/user'
import { AuthStrategy } from '../enums'

export default <ServerRoute[]>[
    {
        method: 'GET',
        path: '/user',
        handler: api.getAll,
        options: {
            auth: AuthStrategy.JwtAccess,
            id: 'user.getAll',
            description: 'get all users',
            tags: ['api', 'user'],
            validate: {
                query: getByEmailSchema
            }
        }
    },
    {
        method: 'GET',
        path: '/user/{userId}',
        handler: api.getById,
        options: {
            auth: AuthStrategy.JwtAccess,
            id: 'user.getById',
            description: 'get user by id',
            tags: ['api', 'user'],
            validate: {
                params: getByIdSchema
            }
        }
    },
    {
        method: 'PUT',
        path: '/user/{userId}',
        handler: api.updateUser,
        options: {
            auth: AuthStrategy.JwtAccess,
            id: 'user.update',
            description: 'update user name and surname',
            tags: ['api', 'user'],
            validate: {
                params: getByIdSchema,
                payload: updateUserSchema
            }
        }
    },
    {
        method: 'GET',
        path: '/user/count',
        handler: api.getNewUsersCount,
        options: {
            auth: AuthStrategy.JwtAccess,
            id: 'user.getCount',
            description: 'get new monthly users count',
            tags: ['api', 'user']
        }
    }
]
