import * as Hapi from '@hapi/hapi';
import { Test, getServerInjectOptions } from './utils';
import { expect, describe, it, beforeAll, afterAll, beforeEach } from '@jest/globals'
import { getUUID } from '../src/server/utils';
import { ICredentials, ISignUpCredentials } from '../src/server/interfaces';

describe('User API', () => {
    let server: Hapi.Server;
    let res: any;

    let password: string = 'Password123!!';

    let access: string;
    let userId: string;

    let email: string = `${getUUID()}@example.com`;

    const signUp: ISignUpCredentials = {
        email,
        password,
    };

    const specialistCred: ICredentials = {
        login: email,
        password,
    };

    beforeAll(async () => {
        server = await Test.start();
        res = await server.inject(
            getServerInjectOptions('/api/auth/registration', 'POST', null, signUp),
        );
    });

    beforeEach(async () => {
        res = await server.inject(
            getServerInjectOptions('/api/auth/login', 'POST', null, specialistCred),
        );

        access = res.result.result.access;
    })

    afterAll(async () => {
        await server.stop();
    });

    it('Get all', async () => {
        res = await server.inject(
            getServerInjectOptions('/api/user', 'GET', access),
        );
        userId = res.result.result.rows[0].id
        expect(res.statusCode).toEqual(200);
    });

    it('Get by ID', async () => {
        res = await server.inject(
            getServerInjectOptions(`/api/user/${userId}`, 'GET', access),
        );

        expect(res.statusCode).toEqual(200);
    });

    it('Update user', async () => {
        res = await server.inject(getServerInjectOptions(`/api/user/${userId}`, 'PUT', access, { firstName: 'firstName', lastName: 'lastName' }));

        expect(res.statusCode).toEqual(200);
    });

    it('User count', async () => {
        res = await server.inject(getServerInjectOptions('/api/user/count', 'GET', access));
        expect(res.statusCode).toEqual(200);
    });
});
